#!/usr/bin/env bash

## Configure script to exit on error ##
set -e

## GLOBAL VARS ##

_FILE_DIR_NAME_="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

rm -rf ./studio3t-install-dir/*
docker build -t studio3t --compress --no-cache -f Dockerfile ./
#docker build -t studio3t --compress -f Dockerfile ./

docker-compose build --no-cache