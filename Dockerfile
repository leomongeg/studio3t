FROM ubuntu:20.10
RUN rm -rf /var/lib/apt/lists/*
RUN apt-get update
RUN apt-get install -y libswt-gtk-4-jni
RUN apt-get install -y libgtk-3-dev
RUN groupadd -g 1000 lyonn
RUN useradd -d /home/lyonn -s /bin/bash -m lyonn -u 1000 -g 1000
USER lyonn
ENV HOME /home/lyonn

COPY . /home/lyonn

WORKDIR /home/lyonn
RUN tar -xvzf studio-3t-linux-x64.tar.gz
#RUN tar -xvzf studio-3t-linux-x64.tar.gz \
#    && chmod +x ./studio-3t-linux-x64.sh \
#    && sh ./studio-3t-linux-x64.sh

#https://github.com/moby/moby/issues/8710
#https://cntnr.io/running-guis-with-docker-on-mac-os-x-a14df6a76efc
#socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
#open -a XQuartz
#docker run -e DISPLAY=192.168.86.21:0 jess/tor-browser
#docker run -v "/tmp/.X11-unix:/tmp/.X11-unix" -e DISPLAY=192.168.86.21:0 -h "$HOSTNAME" -v "$HOME/.Xauthority:/home/lyonn/.Xauthority" studio3t
#docker.for.mac.host.internal
#https://www.xquartz.org/
#CMD /bin/bash
#CMD /usr/bin/firefox
CMD tail -f /dev/null


#192.168.86.249
#docker run -e DISPLAY=host.docker.internal:0 jess/tor-browser
#host.docker.internal
#docker run -v "/tmp/.X11-unix:/tmp/.X11-unix" -e DISPLAY=host.docker.internal:0 -h "$HOSTNAME" -v "$HOME/.Xauthority:/home/lyonn/.Xauthority" studio3t
#docker build -t localhost/studio3t --compress -f Dockerfile ./


#docker build -t studio3t --compress -f Dockerfile ./
#xhost +127.0.0.1
#docker run -v "/tmp/.X11-unix:/tmp/.X11-unix" -e DISPLAY=host.docker.internal:0 -h "$HOSTNAME" -v "$HOME/.Xauthority:/home/lyonn/.Xauthority" studio3t

# 1030  xhost +127.0.0.1
# 1031  docker run -v "/tmp/.X11-unix:/tmp/.X11-unix" -e DISPLAY=host.docker.internal:0 -h "$HOSTNAME" -v "$HOME/.Xauthority:/home/lyonn/.Xauthority" studio3t
# 1032  xhost +127.0.0.1
# 1033  docker run -v "/tmp/.X11-unix:/tmp/.X11-unix" -e DISPLAY=host.docker.internal:0 -h "$HOSTNAME" -v "$HOME/.Xauthority:/home/lyonn/.Xauthority" -p 27018:27017 studio3t
# 1034  docker run -v "/tmp/.X11-unix:/tmp/.X11-unix" -e DISPLAY=host.docker.internal:0 -h "$HOSTNAME" -v "$HOME/.Xauthority:/home/lyonn/.Xauthority" -p 27017:27017 studio3t
# 1035  docker run -v "/tmp/.X11-unix:/tmp/.X11-unix" -e DISPLAY=host.docker.internal:0 -h "$HOSTNAME" -v "$HOME/.Xauthority:/home/lyonn/.Xauthority" -p 27018:27017 studio3t