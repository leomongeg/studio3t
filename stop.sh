#!/usr/bin/env bash

## Configure script to exit on error ##
set -e

## GLOBAL VARS ##

_FILE_DIR_NAME_="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


docker-compose down