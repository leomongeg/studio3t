#!/usr/bin/env bash

## Configure script to exit on error ##
set -e

## GLOBAL VARS ##

_FILE_DIR_NAME_="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


## Script imports ##

xhost +127.0.0.1

docker-compose up -d



echo "  _    _                            _____          _     _ _               _   _   _  "
echo " | |  | |                          / ____|        | |   | (_)             | | | | | | "
echo " | |__| | __ _ _ __  _ __  _   _  | |     ___   __| | __| |_ _ __   __ _  | | | | | | "
echo " |  __  |/ _  |  _ \|  _ \| | | | | |    / _ \ / _  |/ _  | |  _ \ / _  | | | | | | | "
echo " | |  | | (_| | |_) | |_) | |_| | | |___| (_) | (_| | (_| | | | | | (_| | |_| |_| |_| "
echo " |_|  |_|\__,_| .__/| .__/ \__, |  \_____\___/ \__,_|\__,_|_|_| |_|\__, | (_) (_) (_) "
echo "              | |   | |     __/ |                                   __/ |             "
echo "              |_|   |_|    |___/                                   |___/               \n"


