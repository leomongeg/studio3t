# Studio 3t Docker Solution

Este bundle permite ejecutar de manera indefinida el Studio 3t.

## Requerimientos

 - Docker Engine
 - Docker Compose
 - brew gestor de paquetes y dependencias de Mac OS

## Instalación para Mac OS

Para poder ejecutar Studio 3t desde docker dentro del entorno grafico de Mac OS
es necesario instalar algunas dependencias dentro del sistema.


**1 Instalar XQuartz**:
 
```bash
brew install xquartz
```


**2 Configurar XQuartz**:

Una ves intalado, ejecutar el sigiente commando:

```bash
open -a Xquartz
```

Eso ejecutara el programa, ahora proceder a la sección de configuración en el
menu principal XQuartz > Preferences y habiliatar Allow connections from network clients, 
tal y como se muestra en la siguiente imagen:
 
![picture](./docs/1_zMO-bPar1Z1AUUH-O2WBfw.png)



**3 Construir la imagen**

Para construir el proyecto basta con ejectuar el siguiente script:

```bash
bash build.sh
```

## Ejecución

Para poder ejecutar el proyecto, se debe de iniciar el contendor, utilizando el siguiente script:

```bash
export DISPLAY=:0

bash start.sh
```

*Este paso se debe de ejecutar siempre que se reinicie o apague la computador o que se reinicie el docker
engine, o que por algun motivo se detenga el container.


**Installar la Studio 3t**

Simpre la primera vez se debe de instalar el Studio 3t, para instarlo, basta con ejecutar el siguiente script:

```bash
bash install-studio3t.sh
```

Si todo esta bien instalado y configurado, se debe de mostrar el wizard de instalación. Este paso es muy
importante, se debe de cambiar el directorio donde se instalá el Studio 3t, y debe de 
ser: `/home/lyonn/studio3t-install-dir/studio3t` este path se puede seleccionar utilizando el browse del wizard:

![picture](./docs/installwizard.png)

**Iniciar Studio 3t**

Para ejecutar posteriormente el Studio 3T, se debe de ejecutar el siguiente script:

```bash
bash start-studio.sh
```

Para configurar conexiones, en mac el host `host.docker.internal` es equivalente al localhost
de la maquina real:

![picture](./docs/connection.png)


Para detener el contenedor, solo basta con ejecutar el script:

```bash
bash stop.sh
```

## Actualizaciones y renovación del trial

Para actualizar a una nueva version de Studio 3t, descargue el nuevo `studio-3t-linux-x64.tar.gz` y reemplacelo
por el existente. 
Finalmente vuelva a ejectuar todos los pasos desde el punto `3 Construir la imagen`

Lo mismo aplica cuando se vence el trial.